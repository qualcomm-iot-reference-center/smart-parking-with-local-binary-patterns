import cv2
from lbp.localbinarypatterns import LocalBinaryPatterns
from imutils import paths
import multiprocessing
from multiprocessing import Queue
from datetime import datetime
import time
import pickle

desc = LocalBinaryPatterns(24, 8)
rois = [(93, 21, 175, 110), (191, 5, 276, 101), (295, 5, 371, 80), (405, 10, 475, 96),
        (510, 23, 580, 93)]
filename = 'model.sav'
model = pickle.load(open(filename, 'rb'))

def process_rois(frames_queue, model):
    while True:
        frame = frames_queue.get()

        for n, r in enumerate(rois):
            #frame[yInicial:yFinal, xInicial:yFinal]
            gray = cv2.cvtColor(frame[r[1]:r[3], r[0]:r[2]], cv2.COLOR_BGR2GRAY)
            hist = desc.describe(gray)
            prediction = model.predict(hist.reshape(1, -1))
            cv2.rectangle(frame, (r[0], r[1]), (r[2], r[3]),
                          (0, 255, 0) if 'asphalt' in prediction[0] else (0, 0, 255), 3)
        cv2.imshow("Processa", frame)
        cv2.waitKey(10)


vobj = cv2.VideoCapture("rtsp:admin:admin@172.16.100.100:554")

if __name__ == '__main__':
    print('------- STARTING APPLICATION -------')
    frames_queue = Queue(maxsize = 0)
    p = multiprocessing.Process(target = process_rois, args = (frames_queue, model))
    p.start()

    start = datetime.now()
    while True:
        status, frame = vobj.read()
        while status == False:
            status, frame = vobj.read()
        cv2.imshow("Camera capture", frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            frames_queue = None
            break
        end = datetime.now()
        if (end - start).total_seconds() > 5:
            frames_queue.put(frame)
            start = datetime.now()

    vobj.release()
    p.terminate()
    cv2.destroyAllWindows()