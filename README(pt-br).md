# Smart Parking with Local Binary Patterns

Projeto desenvolvido no IoT Reference Center, parceria entre Qualcomm e FACENS. Controle de estacionamento com câmera, utilizando um algoritmo baseado em identificação de texturas em imagens.

# Material utilizado no projeto

 - DragonBoard 410c
 - Câmera IP para filmar o estacionamento
 - Cartão SD (mínimo 4GB)
 - Monitor HDMI
 - (opcional) Pendrive

# Teoria

## Local Binary Patterns

O LBP é um método de descrição de textura, apresentado em um artigo de 2002. A ideia do descritor é padronizar uma região de pixels para criar uma representação da imagem que possa ser processada pelo computador. A entrada do método é uma imagem em escala de cinza. O tamanho da área a ser processado em cada iteração é definido ao redor do pixel central denominado treshold, e o cálculo realizado é simples. A imagem abaixo demonstra a utilização de um kernel 3x3 para processar a imagem:

 <div align="center">
    <figure>
        <img src="Tutorial/lbp_thresholding.jpg">
        <figcaption>LBP treshhold 3x3</figcaption>
    </figure>
</div>

Observe que o critério de classificação adotado é em relação ao treshold: se o valor do pixel central for maior ou igual ao valor do pixel analisado, o descritor adota o valor 1 para o pixel analisado. Caso contrário, o valor é zero.

Após a operação ser realizada em todos os pixels atingidos pelo kernel, um número binário é formado como indicado na imagem abaixo. O número que representa a área é então armazenado num vetor 2D, que guarda os valores correspondentes de todas as áreas processadas pela descrição LBP da imagem.

 <div align="center">
    <figure>
        <img src="Tutorial/lbp_calculation.jpg">
        <figcaption>Exemplo de cálculo do LBP</figcaption>
    </figure>
</div>

O resultado do processo é uma imagem como mostrado abaixo:

 <div align="center">
    <figure>
        <img src="Tutorial/lbp_2d_array.jpg">
        <figcaption>Output do array 2D do LBP</figcaption>
    </figure>
</div>

O LBP é o primeiro processo aplicado nas imagens nesse projeto. As fotos são aquisitadas de um stream da câmera de vídeo, e cada frame é processado com o descritor. Essa etapa facilita a aplicação de um modelo de inteligência artificial para reconhecimento, descrito a seguir. A biblioteca utilizada para o procedimento é a skimage.

## Support Vector Machines

SVM é um modelo de classificação de aprendizado supervisionado que consiste em estabelecer as linhas delimitadoras que melhor dividem o dataset em classes. Observe a imagem abaixo:

 <div align="center">
    <figure>
        <img src="Tutorial/SVM.png">
        <figcaption>Definição de um support vector machine</figcaption>
    </figure>
</div>

A imagem representa um dataset com duas classes, os quadrados e os círculos. Na representação à esquerda, são apresentadas diversas opções de linhas de divisão para a classificação das classes, e todas dividem o dataset de maneira correta. O diferencial do SVM é encontrar a divisão otimizada, representado à direita. Com a otimização, a chance de um novo ponto ser classificado corretamente é alta, e o modelo apresenta resultados tão satisfatórios quanto modelos mais complexos como redes neurais, dependendo do dataset.

No projeto, um modelo SVM da biblioteca scikit-learn é treinado com um dataset construido pela equipe (explicaremos como construir o seu), e é utilizado para classificar as imagens em vagas ocupadas e livres.

# Instalação e execução do projeto


## Hardware Utilizado

 

> Essa aplicação foi executada na plataforma de desenvolvimento DragonBoard410c. A imagem do sistema operacional é a linaro-buster-alip-dragonboard-410c-359.img.gz e pode ser obtida neste link: https://releases.linaro.org/96boards/dragonboard410c/linaro/debian/latest/ 

 

> Versão do kernel: 

 

> Linux qubuntu 4.15.0-36-generic #39-Ubuntu SMP Mon Sep 24 16:19:09 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux

## Dataset e treinamento do modelo

O primeiro passo do projeto é construir um dataset e treinar um modelo. Vamos realizar estes procedimentos em um computador, para que a DragonBoard execute apenas o processo de inferência. Utilizamos uma máquina com Ubuntu-Linux e Python para realizar o projeto.

Inicialmente, você vai precisar de imagens que representem suas classes. Na nossa aplicação, as classes são vagas ocupadas ou livres, então optamos por representá-las com fotos de vagas vazias e fotos de carros ocupando as vagas. Armazenamos as imagens em uma pasta de nome **images**, separadas por categoria em duas subpastas, **asphalt** e **car**. Para o script que escrevemos com intuito de facilitar o processo, é importante que as pastas que armazenam uma classe sejam nomeadas com a label da classe (nossas classes serão chamadas asphalt e car, no exemplo). Para um melhor resultado, é recomendável um dataset grande, então colete quantas imagens você conseguir para representar suas classes.

As bibliotecas utilizadas para o treinamento e o comando para instalação:

```sh

pip install imutils

sudo apt-get install python-opencv

pip install scikit-learn

pip install scikit-image

pip install numpy

```

Se preferir escrever o script você mesmo, o código está disponibilizado abaixo. Caso prefira realizar o download do repositório execute o comando abaixo e rode o código:

### Importante: a pasta do seu dataset deve estar no mesmo diretório e com os mesmos nomes que o código para funcionar sem alterações. Se preferir fazer alterações, os trechos estão indicados no código abaixo.

```sh

git clone https://gitlab.com/qualcomm-iot-reference-center/smart-parking-with-local-binary-patterns.git

cd smart-parking-with-local-binary-patterns
python trainModel.py
```

Código do script trainModel.py:

```python

from imutils import paths
import cv2
from sklearn.svm import LinearSVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from lbp.localbinarypatterns import LocalBinaryPatterns
import pickle

desc = LocalBinaryPatterns(24, 8)
data = []
labels = []

def train():
    # loop over the training images
    for imagePath in paths.list_images('images/training'): #---------SUBSTITUA COM O DIRETÓRIO DO SEU DATASET---------
        # load the image, convert it to grayscale, and describe it
        image = cv2.imread(imagePath)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        hist = desc.describe(gray)
        # extract the label from the image path, then update the
        # label and data lists
        labels.append(imagePath.split('/')[2])
        data.append(hist)

    
    # split the data
    x_train, x_test, y_train, y_test = train_test_split(data, labels, train_size=0.3)

    #create a SVM model and fit the data
    model = LinearSVC(C=10.0, random_state=42)
    model.fit(x_train, y_train)
    print("---------- TRAINING PROCESS FINISHED ----------")
    #report the accuracy of the trained model
    pred = model.predict(x_test)
    print("---------- PRECISION REPORT ----------")
    print(classification_report(pred, y_test))

    # save the model to disk
    filename = 'model.sav'
    pickle.dump(model, open(filename, 'wb'))


if __name__ == '__main__':

    train()

```
Durante a execução, o código vai listar as fotos do seu dataset, criar as labels conforme indicado pelo nome das pastas, e treinar o modelo. Após o treinamento, ele deve retornar no terminal um relatório da precisão do modelo criado, com base no teste feito com algumas imagens do dataset.
Após a execução do código, um arquivo de nome **model.sav** será gerado na mesma pasta. Esse arquivo é o modelo SVM compactado, e precisamos copiá-lo para a DragonBoard para que o projeto funcione. A maneira mais simples é utilizar um pendrive.


## Execução do projeto na DragonBoard


Primeiro, instale as bibliotecas necessárias para o funcionamento, executando os comandos abaixo no terminal da placa.

### Importante: O precesso de instalação exige memória RAM superior à instalada na placa, fazendo necessária a utilização de uma memória SWAP. Veja como fazer uso do cartão SD [neste tutorial](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard.md).



```sh

pip install imutils

sudo apt-get install python-opencv

pip install scikit-image

pip install multiprocessing

pip install numpy

pip install scikit-learn

```

Clone o diretório para a placa, executando os comandos:


```sh

git clone https://gitlab.com/qualcomm-iot-reference-center/smart-parking-with-local-binary-patterns.git

cd smart-parking-with-local-binary-patterns

```

Agora será necessário copiar o pickle gravado anteriormente para a placa, no diretório clonado. A maneira mais fácil é utilizar um pendrive e o comando *cp <caminho do arquivo> <caminho do destino>* no terminal da placa.

No código do projeto main.py presente no diretório, são definidos os parâmetros da câmera. No nosso projeto utilizamos uma câmera IP, então o acesso é feito através do IP, como no trecho destacado:

```python

import cv2
from lbp.localbinarypatterns import LocalBinaryPatterns
from imutils import paths
import multiprocessing
from multiprocessing import Queue
from datetime import datetime
import time
import pickle

desc = LocalBinaryPatterns(24, 8)
rois = [(93, 21, 175, 110), (191, 5, 276, 101), (295, 5, 371, 80), (405, 10, 475, 96),
        (510, 23, 580, 93)]
filename = 'model.sav'
model = pickle.load(open(filename, 'rb'))

def process_rois(frames_queue, model):
    while True:
        frame = frames_queue.get()

        for n, r in enumerate(rois):
            #frame[yInicial:yFinal, xInicial:yFinal]
            gray = cv2.cvtColor(frame[r[1]:r[3], r[0]:r[2]], cv2.COLOR_BGR2GRAY)
            hist = desc.describe(gray)
            prediction = model.predict(hist.reshape(1, -1))
            cv2.rectangle(frame, (r[0], r[1]), (r[2], r[3]),
                          (0, 255, 0) if 'asphalt' in prediction[0] else (0, 0, 255), 3)
        cv2.imshow("Processa", frame)
        cv2.waitKey(10)


vobj = cv2.VideoCapture("rtsp:admin:admin@172.16.100.100:554") #------SUBSTITUA COM O PARÂMETRO DA SUA CÂMERA

if __name__ == '__main__':
    print('------- STARTING APPLICATION -------')
    frames_queue = Queue(maxsize = 0)
    p = multiprocessing.Process(target = process_rois, args = (frames_queue, model))
    p.start()

    start = datetime.now()
    while True:
        status, frame = vobj.read()
        while status == False:
            status, frame = vobj.read()
        cv2.imshow("Camera capture", frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            frames_queue = None
            break
        end = datetime.now()
        if (end - start).total_seconds() > 5:
            frames_queue.put(frame)
            start = datetime.now()

    vobj.release()
    p.terminate()
    cv2.destroyAllWindows()

```

Com o código readequado para o seu uso, e o modelo gravado já no mesmo diretório, execute a aplicação com:

```sh
python main.py
```
# Exemplos

Nessa seção, apresentamos imagens do projeto em funcionamento no Centro de Referência.


 <div align="center">
    <figure>
        <img src="Tutorial/free.jpeg">
        <figcaption>Controle de vagas</figcaption>
    </figure>
</div>


 <div align="center">
    <figure>
        <img src="Tutorial/parking.jpeg">
        <figcaption>Antes da ocupação de uma vaga</figcaption>
    </figure>
</div>


 <div align="center">
    <figure>
        <img src="Tutorial/car.jpeg">
        <figcaption>Controle com status atualizado</figcaption>
    </figure>
</div>

# Referências

## SVM - TowardsDataScience 
https://towardsdatascience.com/support-vector-machine-vs-logistic-regression-94cc2975433f

## LBP - PyImageSearch
https://www.pyimagesearch.com/2015/12/07/local-binary-patterns-with-python-opencv/