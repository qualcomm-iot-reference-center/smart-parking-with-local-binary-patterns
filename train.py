from imutils import paths
import cv2
from sklearn.svm import LinearSVC
from lbp.localbinarypatterns import LocalBinaryPatterns
import pickle

desc = LocalBinaryPatterns(24, 8)
data = []
labels = []

def train():
    # loop over the training images
    for imagePath in paths.list_images('images/training'):
        # load the image, convert it to grayscale, and describe it
        image = cv2.imread(imagePath)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        hist = desc.describe(gray)
        # extract the label from the image path, then update the
        # label and data lists
        labels.append(imagePath.split('/')[2])
        data.append(hist)
    # train a Linear SVM on the data
    model = LinearSVC(C=10.0, random_state=42)
    model.fit(data, labels)
    print("---------- TRAINING PROCESS FINISHED ----------")
    # save the model to disk
    filename = 'model.sav'
    pickle.dump(model, open(filename, 'wb'))


if __name__ == '__main__':

    train()
