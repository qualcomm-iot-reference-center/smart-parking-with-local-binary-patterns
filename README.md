# Smart Parking with Local Binary Patterns

Project developed in the IoT Reference Center, a partnership between Qualcomm and FACENS. Parking control, using an camera and algorithm based on identification of textures in the images.

# Material used in the project

 - DragonBoard 410c
 - IP camera to shoot the parking
 - SD card (at least 4GB)
 - HDMI monitor
 - (optional) Pendrive

# Theory

## Local Binary Patterns

LBP is a method of texture describing, presented in a 2002 article. The idea of ​​the descriptor is to standardize a region of pixels to create a representation of the image that can be processed by the computer. The method entry is a grayscale image. The size of the area to be processed at each iteration is defined around the central pixel called treshold, and the calculation performed is simple. The image below demonstrates the use of a 3x3 kernel to process the image:

 <div align="center">
    <figure>
        <img src="Tutorial/lbp_thresholding.jpg">
        <figcaption>LBP treshhold 3x3</figcaption>
    </figure>
</div>

Note that the classification criterion adopted is related to the treshold: if the value of the central pixel is greater than or equal to the value of the analyzed pixel, the descriptor adopts the value 1 for the analyzed pixel. Otherwise, the value is zero.

After the operation is performed on all pixels affected by the kernel, a binary number is formed as indicated in the image below. The number representing the area is then stored in a 2D vector, which stores the corresponding values ​​of all areas processed by the LBP description of the image.

 <div align="center">
    <figure>
        <img src="Tutorial/lbp_calculation.jpg">
        <figcaption>LBP calculation example</figcaption>
    </figure>
</div>

The result of the process is an image as demonstrated below:

 <div align="center">
    <figure>
        <img src="Tutorial/lbp_2d_array.jpg">
        <figcaption>LBP 2D array output</figcaption>
    </figure>
</div>

LBP is the first process applied to images in this project. Photos are taken from a stream of the camera, and each frame is rendered with the descriptor. This step facilitates the application of an artificial intelligence model for recognition, described below. The library used for the procedure is the skimage.

## Support Vector Machines

SVM is a supervised learning classification model that consists of establishing the bounding lines that best divide the dataset into classes. Look at the image below:

 <div align="center">
    <figure>
        <img src="Tutorial/SVM.png">
        <figcaption>Support vector machine definition</figcaption>
    </figure>
</div>

The image represents a dataset with two classes, squares and circles. In the representation on the left, several split line options are presented for class classification, and all divide the dataset correctly. The differential of the SVM is to find the optimized division, represented to the right. With optimization, the chance of a new point being correctly sorted is high, and the model presents results as satisfying as more complex models such as neural networks, depending on the dataset.

In the project, a SVM model of the scikit-learn library is trained with a dataset constructed by the team (we'll explain how to build your), and is used to classify the images into occupied and free spaces.

# Project installation


## Hardware

 

> This application was run on the DragonBoard410c development platform. The image of the operating system is linaro-buster-alip-dragonboard-410c-359.img.gz and can be obtained at this link: https://releases.linaro.org/96boards/dragonboard410c/linaro/debian/latest/ 

 

> Kernel version: 

 

> Linux qubuntu 4.15.0-36-generic #39-Ubuntu SMP Mon Sep 24 16:19:09 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux

## Dataset and training the model

The first step of the project is to build a dataset and train a model. Let's perform these procedures on a computer, so that DragonBoard only performs the inference process. We used a machine with Ubuntu-Linux and Python to carry out the project.

Initially, you will need images that represent your classes. In our application, the classes are occupied or free spaces, so we chose to represent them with pictures of empty spaces and pictures of cars occupying the parking slots. We store the images in a folder named **images**, separated by category in two subfolders, **asphalt** and **car**. For the script that we write in order to facilitate the process, it is important that the folders that store a class are named with the label of the class (our classes will be called asphalt and car, in the example). For a better result, a large dataset is recommended, so collect as many images as you can to represent your classes.

The libraries used for training and installation commands:

```sh

pip install imutils

sudo apt-get install python-opencv

pip install scikit-learn

pip install scikit-image

pip install numpy

```

If you prefer to write the script yourself, the code is provided below. If you prefer to download the repository execute the command below and run the code:

### Important: The folder in your dataset must be in the same directory and with the same names as the code to work without changes. If you prefer to make changes, the sections are indicated in the code below.

```sh

git clone https://gitlab.com/qualcomm-iot-reference-center/smart-parking-with-local-binary-patterns.git

cd smart-parking-with-local-binary-patterns
python trainModel.py
```

Script code - trainModel.py:

```python

from imutils import paths
import cv2
from sklearn.svm import LinearSVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from lbp.localbinarypatterns import LocalBinaryPatterns
import pickle

desc = LocalBinaryPatterns(24, 8)
data = []
labels = []

def train():
    # loop over the training images
    for imagePath in paths.list_images('images/training'): #---------CHANGE YOUR FOLDER PATH HERE---------
        # load the image, convert it to grayscale, and describe it
        image = cv2.imread(imagePath)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        hist = desc.describe(gray)
        # extract the label from the image path, then update the
        # label and data lists
        labels.append(imagePath.split('/')[2])
        data.append(hist)

    
    # split the data
    x_train, x_test, y_train, y_test = train_test_split(data, labels, train_size=0.3)

    #create a SVM model and fit the data
    model = LinearSVC(C=10.0, random_state=42)
    model.fit(x_train, y_train)
    print("---------- TRAINING PROCESS FINISHED ----------")
    #report the accuracy of the trained model
    pred = model.predict(x_test)
    print("---------- PRECISION REPORT ----------")
    print(classification_report(pred, y_test))

    # save the model to disk
    filename = 'model.sav'
    pickle.dump(model, open(filename, 'wb'))


if __name__ == '__main__':

    train()

```
During execution, the code will list the photos in your dataset, create the labels as indicated by the name of the folders, and train the SVM model. After the training, it should return in the terminal a report of the accuracy of the created model, based on the test done with some images of the dataset.
After executing the code, a file named **model.sav** will be generated in the same folder. This file is the compressed SVM template, and we need to copy it to DragonBoard for the project to work. The simplest way is to use a pendrive:

#### In the host machine, copy the file to the pen drive:

```sh
cp model.sav /media/<USER>/<pendrive>

```

## Executing the project on DragonBoard


First, install the libraries required for operation by running the commands below on the terminal board.

### Important: The installation process requires more RAM than the one installed on the board, making it necessary to use a SWAP memory. Here's how to use the SD card [link](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard.md).



```sh

pip install imutils

sudo apt-get install python-opencv

pip install scikit-image

pip install multiprocessing

pip install numpy

pip install scikit-learn

```

Clone the directory to the card by running the commands:


```sh

git clone https://gitlab.com/qualcomm-iot-reference-center/smart-parking-with-local-binary-patterns.git

cd smart-parking-with-local-binary-patterns

```

#### Copy the model file from the pendrive to the board, executing:

```sh
cp /media/<USER>/<pendrive>/model.sav .
```

In the main.py project code in the directory, the camera parameters are defined. In our project we use an IP camera, so access is done through the IP, as in the highlighted section:

```python

import cv2
from lbp.localbinarypatterns import LocalBinaryPatterns
from imutils import paths
import multiprocessing
from multiprocessing import Queue
from datetime import datetime
import time
import pickle

desc = LocalBinaryPatterns(24, 8)
rois = [(93, 21, 175, 110), (191, 5, 276, 101), (295, 5, 371, 80), (405, 10, 475, 96),
        (510, 23, 580, 93)]
filename = 'model.sav' #---------YOUR FILE PATH HERE
model = pickle.load(open(filename, 'rb'))

def process_rois(frames_queue, model):
    while True:
        frame = frames_queue.get()

        for n, r in enumerate(rois):
            #frame[yInicial:yFinal, xInicial:yFinal]
            gray = cv2.cvtColor(frame[r[1]:r[3], r[0]:r[2]], cv2.COLOR_BGR2GRAY)
            hist = desc.describe(gray)
            prediction = model.predict(hist.reshape(1, -1))
            cv2.rectangle(frame, (r[0], r[1]), (r[2], r[3]),
                          (0, 255, 0) if 'asphalt' in prediction[0] else (0, 0, 255), 3)
        cv2.imshow("Processa", frame)
        cv2.waitKey(10)


vobj = cv2.VideoCapture("rtsp:admin:admin@172.16.100.100:554") #------YOUR CAMERA ADDR HERE

if __name__ == '__main__':
    print('------- STARTING APPLICATION -------')
    frames_queue = Queue(maxsize = 0)
    p = multiprocessing.Process(target = process_rois, args = (frames_queue, model))
    p.start()

    start = datetime.now()
    while True:
        status, frame = vobj.read()
        while status == False:
            status, frame = vobj.read()
        cv2.imshow("Camera capture", frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            frames_queue = None
            break
        end = datetime.now()
        if (end - start).total_seconds() > 5:
            frames_queue.put(frame)
            start = datetime.now()

    vobj.release()
    p.terminate()
    cv2.destroyAllWindows()

```

With the code re-adapted for your use, and the model already recorded in the same directory, run the application with:

```sh
python main.py
```
# Examples

In this section, we present images of the project in operation in the Reference Center.


 <div align="center">
    <figure>
        <img src="Tutorial/free.jpeg">
        <figcaption>Parking control</figcaption>
    </figure>
</div>


 <div align="center">
    <figure>
        <img src="Tutorial/parking.jpeg">
        <figcaption>Before occupation</figcaption>
    </figure>
</div>


 <div align="center">
    <figure>
        <img src="Tutorial/car.jpeg">
        <figcaption>Status update</figcaption>
    </figure>
</div>

# References

## SVM - TowardsDataScience 
https://towardsdatascience.com/support-vector-machine-vs-logistic-regression-94cc2975433f

## LBP - PyImageSearch
https://www.pyimagesearch.com/2015/12/07/local-binary-patterns-with-python-opencv/